import pandas as pd

s = pd.Series([1, 2, 3, 4]) # index陣列顯示
print(s)

zip_codes ={"100":"中正區","103":"大同區","104":"中山區","105":"松山區","106":"大安區","108":"萬華區","110":"信義區","111":"士林區","112":"北投區","114":"內湖區","115":"南港區","116":"文山區"}
T=pd.Series(zip_codes)
print(T)
print(T[3])    #僅取得第4項


taiwan ={'city':['台北市','新北市','桃園市','台中市','台南市','高雄市'],
        'pop':[2631083, 4024539, 2255753, 2816741, 1878845, 2773401],
        'area':[271.7997, 2052.5667, 1220.9540, 2214.8968, 2191.6531, 2951.8524],}

T1=pd.DataFrame(taiwan) #取taiwan資料並建立dataFrame表格
print(T1)

#載入csv黨  df = pd.read_csv('檔案名稱')

T2=pd.DataFrame(taiwan).head(3) #取taiwan前3個row並建立表格
print(T2)